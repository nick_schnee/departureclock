document.addEventListener("DOMContentLoaded", function(event) {

  var connections = [];
  var lastfetchTS = 0;
  var initialdrawdone = false;


  // match radious of the inner circle
  var RADIUS = 35;
  var CIRCUMFERENCE = 2 * Math.PI * RADIUS;
  var progressValue = document.querySelector('.progress_seconds');
  progressValue.style.strokeDasharray = CIRCUMFERENCE;

  // match radius of the outer circle
  var radiusminutes = 54;
  var circminutes = 2 * Math.PI * radiusminutes;
  var progressValueMinutes = document.querySelector('.progress_minutes');

  progressValueMinutes.style.strokeDasharray = circminutes;

  // 100 times per second, set the current value of minutes and seconds
  setInterval(function(){

    var seconds = getSeconds();
    var progress = seconds / 60 * 100;
    applyprogress(progress);

    var minutes = getMinutes();
    var progressminutes = minutes / 60 * 100;
    applyprogressminutes(progressminutes);

  }, 10);

  // listen to live input changes in the FROM field
  document.querySelector('#from').addEventListener('input', function (evt) {
    console.log("listening!");

    var query = document.querySelector('#from').value;

    getstationfrom(query)

  });

  document.querySelector('#to').addEventListener('input', function (evt) {
    console.log("listening!");

    var query = document.querySelector('#to').value;

    getstationto(query)

  });

  getconnections();

















  // get all connections when localstorage is set
  function getconnections(){

    if (localStorage.getItem("from") !== null && localStorage.getItem("to") !== null){

      // console.log("wallah");

      axios.get("http://transport.opendata.ch/v1/connections?from=" + localStorage.getItem("from") +"&to="+ localStorage.getItem("to") +"&limit=16&fields[]=connections/from/departure&fields[]=connections/from/departureTimestamp")

      .then(response => {

        connections = response.data.connections;

        console.log(connections);

        lastfetchTS = getTimestamp();

        drawdepartures();

        if (!initialdrawdone){

          initialdraw();

        }


      }); // axios ende

    }

  }


  function drawdepartures(){

    var intervaldraw = setInterval(function(){

      cleardraw();

      var currentTS = getTimestamp();

      connections.forEach(function(item) {

        var departureTS = item.from.departureTimestamp;
        var departureMinutes = item.from.departure;

        departureMinutes = departureMinutes.slice(14, 16);

        // console.log(departureMinutes);

        if ( departureTS > currentTS && departureTS < (currentTS + 3600) ){

          // console.log("yes!");

          document.querySelector("#L" + departureMinutes).style.strokeWidth = "1";


        } else {

          // check if the last saved connection is less than one hour ahead, if so, try fetch
          if ((currentTS + 3600) > connections[connections.length-1].from.departureTimestamp){

            // console.log("let's fetch! ");
            console.log("differenz: " + (currentTS - lastfetchTS));

            // check if a fetch has been made in the last 5 minutes
            if (currentTS - lastfetchTS > 300){

              getconnections();
              clearInterval(intervaldraw);

            } else {

            }


          } else {

            console.log ("no hurry!");

          }

        }

      });

      // console.log("gedooont!");

    }, 1000);

  }


  // get possible station names
  function getstationfrom(query){

    axios.get("http://transport.opendata.ch/v1/locations?query=" + query)

    .then(response => {

      // console.log(response.data.stations);

      // remove all elements in the list first
      document.querySelector('#list').innerHTML = "";

      response.data.stations.forEach(function(item) {
        // Create a new <option> element.

        var listelement = document.createElement('li');
        listelement.innerHTML = item.name;
        listelement.id = item.id;
        listelement.classList.add("list-from");


        document.querySelector('#list').appendChild(listelement);

      });

      // listen for click on the just added elements
      document.querySelectorAll('.list-from')
      .forEach(item => {
        item.addEventListener('click', function() {

          // save ID to localstorage here
          console.log(this.id);
          localStorage.setItem("from", this.id);

          getconnections();
          initialdraw();

        })

      });

    });

  }

  function getstationto(query){

    axios.get("http://transport.opendata.ch/v1/locations?query=" + query)

    .then(response => {

      // console.log(response.data.stations);

      // remove all elements in the list first
      document.querySelector('#list-to').innerHTML = "";

      response.data.stations.forEach(function(item) {
        // Create a new <option> element.

        var listelement = document.createElement('li');
        listelement.innerHTML = item.name;
        listelement.id = item.id;
        listelement.classList.add("list-to");


        document.querySelector('#list-to').appendChild(listelement);

      });

      // listen for click on the just added elements
      document.querySelectorAll('.list-to')
      .forEach(item => {
        item.addEventListener('click', function() {


          // save ID to localstorage here
          console.log(this.id);
          localStorage.setItem("to", this.id);

          getconnections();
          initialdraw();

        })

      });

    });

  }

  function initialdraw() {

    // draw 60 departures and make them invisible
    for(i = 0; i < 60; i++){

      if (i < 10){
        document.querySelector("#L0" + i).style.transform += "rotate(calc(" + i + " * 6deg))";
        document.querySelector("#L0" + i).style.strokeWidth += "0";

      } else {

        document.querySelector("#L" + i).style.transform += "rotate(calc(" + i + " * 6deg))";
        document.querySelector("#L" + i).style.strokeWidth += "0";

      }

      initialdrawdone = true;

    } // finish drawing 60 departures

  }

  function cleardraw(){

    // console.log("clearing");

    for(i = 0; i < 60; i++){

      if (i < 10){
        document.querySelector("#L0" + i).style.strokeWidth = "0";

      } else {
        document.querySelector("#L" + i).style.strokeWidth = "0";

      }

    } // finish drawing 60 departures


  }

  function applyprogress(value) {
    var progress = value / 100;
    var dashoffset = CIRCUMFERENCE * (1 - progress);

    progressValue.style.strokeDashoffset = dashoffset;
  }

  function applyprogressminutes(value) {
    var progress = value / 100;
    var dashoffset = circminutes * (1 - progress);
    progressValueMinutes.style.strokeDashoffset = dashoffset;
  }


  function getSeconds() {
    var d = new Date();
    var n = d.getSeconds();
    var m = d.getMilliseconds();

    // console.log(m);
    // console.log(n + (m/1000));

    return n + (m/1000);
  }

  function getMinutes() {
    var d = new Date();
    var n = d.getMinutes();

    return n;
  }

  function getTimestamp() {
    var d = Math.floor(Date.now()/1000);
    // console.log(d);

    return d;
  }

});
